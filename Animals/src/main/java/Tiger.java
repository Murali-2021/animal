public class Tiger implements Animal{

    private String color;
    private int weight;

    public Tiger(){
        this.color = "yellow";
        this.weight = 50;
    }

    public Tiger(String color, int weight){
        this.color = color;
        this.weight = weight;
    }
    @Override
    public String getColor() {
        return color;
    }

    @Override
    public int getWeight() {
        return weight;
    }

    @Override
    public void display() {
        System.out.println("Tiger's Color : "+getColor()+" and "+"Weight : "+getWeight());
        System.out.println("Changes made in GitLab");
        System.out.println("Changes made in Intellij");
    }
}
