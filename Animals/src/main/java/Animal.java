public interface Animal {

    String getColor();
    int getWeight();
    void display();
}
