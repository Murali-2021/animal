public class Cat implements Animal {

    private String color;
    private int weight;

    public Cat(){
        this.color = "black";
        this.weight = 10;
    }

    public Cat(String color, int weight){
        this.color = color;
        this.weight = weight;
    }

    @Override
    public String getColor() {
        return color;
    }

    @Override
    public int getWeight() {
        return weight;
    }

    @Override
    public void display() {
        System.out.println("Cat's Color : "+getColor()+" and "+"Weight : "+getWeight());
    }


}
